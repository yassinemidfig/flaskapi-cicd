tags= {
  "env"= "qual"
  "createdBy"= "terraform"
}
api_staging_bucket = "flaskapi-images-bucket-101"
api_terraform_state =  "flaskapi-terraform-state-102"
python_webserver_image = "registry.gitlab.com/yassinemajidi/flaskapi-cicd:latest"
